package com.intech_global.intechplayer;

import android.app.Application;
import android.content.Context;

/**
 * Created by vladimir on 02.04.16.
 */
public class IntechApplication extends Application {

    private static Application mApp = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
    }

    public static Context context() { return mApp.getApplicationContext(); }
}
