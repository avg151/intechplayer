package com.intech_global.intechplayer;

/**
 * Created by vladimir on 03.04.16.
 */

import android.app.IntentService;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;

public class PlayerService extends IntentService {

    public static final String DEMO_URL  = "demoUrl";
    public static final String RECEIVER  = "receiver";
    public static final int RECEIVER_SET_TOTAL  = 0;
    public static final int RECEIVER_SET_CURRENT  = 1;
    public static final String RECEIVER_VALUE_TOTAL  = "total";
    public static final String RECEIVER_VALUE_CURRENT  = "current";

    static MediaPlayer player;
    Handler mHandler;

    public PlayerService() { super("PlayerService"); }

    @Override
    protected void onHandleIntent(Intent intent) {
        String demoUrl = intent.getStringExtra(DEMO_URL);
        final ResultReceiver receiver = intent.getParcelableExtra(RECEIVER);

        final Bundle resultData = new Bundle();
        try {
            if (player != null) {
                player.stop();
            }
            player = new MediaPlayer();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(demoUrl);
            player.prepare();
            player.start();

            resultData.putInt(RECEIVER_VALUE_TOTAL, player.getDuration() / 1000);
            receiver.send(0, resultData);

             mHandler = new Handler(Looper.getMainLooper());

            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if(player != null) {
                        resultData.putInt(RECEIVER_VALUE_CURRENT, player.getCurrentPosition() / 1000);
                        receiver.send(1, resultData);
                    }
                    mHandler.postDelayed(this, 1000);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void runOnUiThread(Runnable action) {
        action.run();
    }

    public static void onPlay() {
        player.start();
    }

    public static void onPause() {
        player.pause();
    }

    public static void onStop() {
        player.pause();
        player.seekTo(0);
    }
}
