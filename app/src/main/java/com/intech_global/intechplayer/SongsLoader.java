package com.intech_global.intechplayer;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.intech_global.intechplayer.models.Song;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vladimir on 02.04.16.
 */
public class SongsLoader {

    RequestQueue mQueue;
    OnSongsLoaderListener loaderListener;
    private int from;
    private final String URL = "https://api-content-beeline.intech-global.com/public/marketplaces/1/tags/4/melodies";

    public SongsLoader() {
        mQueue = VolleyManager.getInstance().getRequestQueue();
        from = 0;
    }

    public void getSongsList() {
        requestSongs("?limit=20&from=" + from * 20);
    }

    public void loadNextPage() {
        from++;
        getSongsList();
    }

    private void requestSongs(String urlPath) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL + urlPath,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            parseSongs(response.getJSONArray("melodies"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mQueue.add(request);
    }

    private void parseSongs(JSONArray jsonSongs) {
        ArrayList<Song> songs = new ArrayList<>();
        for (int i = 0; i < jsonSongs.length(); i++) {
            try {
                songs.add(new Song((JSONObject) jsonSongs.get(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        loaderListener.onSongLoadingComplete(songs);
    }

    public interface OnSongsLoaderListener {
        void onSongLoadingComplete(ArrayList<Song> songs);
    }

    public void setOnSongListener(OnSongsLoaderListener listener) {
        loaderListener = listener;
    }
}
