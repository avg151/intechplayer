package com.intech_global.intechplayer;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by vladimir on 02.04.16.
 */
public class VolleyManager {
    private static VolleyManager mInstance = null;
    private RequestQueue mRequestQueue;

    private VolleyManager() {
        mRequestQueue = Volley.newRequestQueue(IntechApplication.context());
    }

    public static VolleyManager getInstance() {
        if(mInstance == null){
            mInstance = new VolleyManager();
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() { return this.mRequestQueue; }
}
