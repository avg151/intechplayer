package com.intech_global.intechplayer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.intech_global.intechplayer.PlayerService;
import com.intech_global.intechplayer.R;
import com.squareup.picasso.Picasso;

import static com.intech_global.intechplayer.PlayerService.*;
import static com.intech_global.intechplayer.activities.PlayerActivity.PlayerState.*;

public class PlayerActivity extends AppCompatActivity {

    public static final String IMAGE_URL = "imageUrl";
    public static final String DEMO_URL  = "demoUrl";
    public static final String TITLE     = "title";
    public static final String ARTIST    = "artist";

    ProgressBar progressBar;
    ImageButton playPauseButton;
    ImageButton stopButton;
    TextView startTimeTV;
    TextView stopTimeTV;

    enum PlayerState {PAUSE_STATE, PLAY_STATE, STOP_STATE}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        playPauseButton = (ImageButton) findViewById(R.id.pause_button);
        stopButton = (ImageButton) findViewById(R.id.stop_button);
        progressBar = (ProgressBar) findViewById(R.id.player_progress);
        startTimeTV = (TextView) findViewById(R.id.player_start_time);
        stopTimeTV = (TextView) findViewById(R.id.player_stop_time);

        if (playPauseButton != null) {
            playPauseButton.setTag(PAUSE_STATE);
        }

        if (stopButton != null) {
            stopButton.setTag(STOP_STATE);
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String imageUrl = extras.getString(IMAGE_URL);
            String demoUrl = extras.getString(DEMO_URL);
            String title = extras.getString(TITLE);
            String artist = extras.getString(ARTIST);

            TextView playerTitle = (TextView)findViewById(R.id.player_title);
            if (playerTitle != null) {
                playerTitle.setText(title);
            }

            TextView playerAuthor = (TextView)findViewById(R.id.player_author);
            if (playerAuthor != null) {
                playerAuthor.setText(artist);
            }

            Picasso.with(this).load(imageUrl).into((ImageView) findViewById(R.id.playerIcon));

            Intent playerService = new Intent(this, PlayerService.class);
            playerService.putExtra(DEMO_URL, demoUrl);
            playerService.putExtra(RECEIVER, new Receiver(new Handler()));
            startService(playerService);
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_up);
    }

    public void onPlayerAction(View view) {
        switch ((PlayerState) view.getTag()) {
            case PAUSE_STATE:
                PlayerService.onPause();
                playPauseButton.setImageResource(R.drawable.ic_play_arrow_black_48dp);
                playPauseButton.setTag(PLAY_STATE);
                break;
            case PLAY_STATE:
                PlayerService.onPlay();
                playPauseButton.setImageResource(R.drawable.ic_pause_black_48dp);
                playPauseButton.setTag(PAUSE_STATE);
                break;
            case STOP_STATE:
                PlayerService.onStop();
                playPauseButton.setImageResource(R.drawable.ic_play_arrow_black_48dp);
                playPauseButton.setTag(PLAY_STATE);
                break;
        }
    }

    private class Receiver extends ResultReceiver {
        public Receiver(Handler handler) { super(handler); }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            if (resultCode == RECEIVER_SET_TOTAL) {
                int total = resultData.getInt(RECEIVER_VALUE_TOTAL);
                progressBar.setMax(total);
                stopTimeTV.setText(toReadableTime(total));
            } else if (resultCode == RECEIVER_SET_CURRENT) {
                int current = resultData.getInt(RECEIVER_VALUE_CURRENT);
                progressBar.setProgress(current);
                startTimeTV.setText(toReadableTime(current));
            }
        }

        private String toReadableTime(int intTime) {
            int hours = intTime / 3600;
            int remainder = intTime - hours * 3600;
            int mins = remainder / 60;
            remainder = remainder - mins * 60;
            int secs = remainder;

            String time = "" + secs;
            if (secs < 10) {
                time = "0" + time;
            }

            time = "" + mins + ":" + time;

            if (hours != 0) {
                if (mins < 10) {
                    time = "0" + time;
                }

                time = "" + hours + ":" + time;
            }
            return time;
        }
    }
}
