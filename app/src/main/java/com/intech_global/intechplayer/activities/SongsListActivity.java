package com.intech_global.intechplayer.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.intech_global.intechplayer.NetworkStateReceiver;
import com.intech_global.intechplayer.R;
import com.intech_global.intechplayer.SongsLoader;
import com.intech_global.intechplayer.models.Song;
import com.intech_global.intechplayer.view.no_connection.NoConnectionFragment;
import com.intech_global.intechplayer.view.song.grid.SongsGridAdapter;
import com.intech_global.intechplayer.view.song.grid.SongsGridAdapter.GridLoadingListener;
import com.intech_global.intechplayer.view.song.grid.SongsGridFragment;
import com.intech_global.intechplayer.view.song.list.SongsListAdapter;
import com.intech_global.intechplayer.view.song.list.SongsListAdapter.ListLoadingListener;
import com.intech_global.intechplayer.view.song.list.SongsListFragment;

import java.util.ArrayList;

import static com.intech_global.intechplayer.NetworkStateReceiver.NetworkStateReceiverListener;
import static com.intech_global.intechplayer.SongsLoader.OnSongsLoaderListener;

public class SongsListActivity extends AppCompatActivity
        implements OnSongsLoaderListener, NetworkStateReceiverListener,
        ListLoadingListener, GridLoadingListener {

    enum ViewRepresentation {LIST, GRID}

    SongsListFragment songsListFragment;
    SongsGridFragment songsGridFragment;
    SongsLoader songsLoader;
    SongsListAdapter songsListAdapter;
    SongsGridAdapter songsGridAdapter;

    static ViewRepresentation viewRepresentation;

    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs_list);

        NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver(this);
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        if (viewRepresentation == null) {
            viewRepresentation = ViewRepresentation.LIST;
        }

    }

    @Override
    public void onSongLoadingComplete(ArrayList<Song> songs) {
        switch (viewRepresentation) {
            case LIST:
                if (songsListAdapter == null) {
                    songsListAdapter = new SongsListAdapter(this, songs);
                    songsListAdapter.setPageLoadingListener(this);
                    songsListFragment.setListAdapter(songsListAdapter);
                } else {
                    songsListAdapter.add(songs);
                    songsListAdapter.notifyDataSetChanged();
                }
                break;
            case GRID:
                if (songsGridAdapter == null) {
                    songsGridAdapter = new SongsGridAdapter(this, songs);
                    songsGridAdapter.setPageLoadingListener(this);
                    songsGridFragment.setAdapter(songsGridAdapter);
                } else {
                    songsGridAdapter.add(songs);
                    songsGridAdapter.notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    public void networkAvailable() {
        songsLoader = new SongsLoader();
        songsLoader.setOnSongListener(this);
        songsLoader.getSongsList();

        switch (viewRepresentation) {
            case LIST:
                songsListFragment = new SongsListFragment();
                replaceWithFragment(songsListFragment);
                break;
            case GRID:
                songsGridFragment = new SongsGridFragment();
                replaceWithFragment(songsGridFragment);
                break;
        }
        updateToolbarIcon();
    }

    @Override
    public void networkUnavailable() {
        replaceWithFragment(new NoConnectionFragment());

        if (menu != null) {
            menu.findItem(R.id.menu_list).setVisible(false);
            menu.findItem(R.id.menu_grid).setVisible(false);
        }

        songsListAdapter = null;
        songsGridAdapter = null;
    }

    private void replaceWithFragment(Fragment fragment) {
        FragmentTransaction fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, fragment);
        fTrans.commit();
    }

    @Override
    public void onLoadNextPage() {
        songsLoader.loadNextPage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.songs_list_menu, menu);
        this.menu = menu;
        updateToolbarIcon();
        return true;
    }

    private void updateToolbarIcon() {
        if (menu != null) {
            switch (viewRepresentation) {
                case LIST:
                    menu.findItem(R.id.menu_list).setVisible(false);
                    menu.findItem(R.id.menu_grid).setVisible(true);
                    break;
                case GRID:
                    menu.findItem(R.id.menu_list).setVisible(true);
                    menu.findItem(R.id.menu_grid).setVisible(false);
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        songsListAdapter = null;
        songsGridAdapter = null;
        switch (item.getItemId()) {
            case R.id.menu_list:
                viewRepresentation = ViewRepresentation.LIST;
                break;

            case R.id.menu_grid:
                viewRepresentation = ViewRepresentation.GRID;
                break;
        }
        updateToolbarIcon();
        networkAvailable();
        return super.onOptionsItemSelected(item);
    }
}