package com.intech_global.intechplayer.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vladimir on 02.04.16.
 */
public class Song {

    private int     id;
    private String  title;
    private int     artistId;
    private String  artist;
    private int     code;
    private int     smsNumber;
    private int     price;
    private double  fPrice;
    private int     prolongationPrice;
    private double  fProlongationPrice;
    private String  purchasePeriod;
    private int     position;
    private String  picUrl;
    private String  demoUrl;
    private SongTag tags;
    private boolean active;
    private int     relevance;
    private int     melodyId;

    public Song(JSONObject jsonSong) {
        try {
            id                 = jsonSong.getInt("id");
            title              = jsonSong.getString("title");
            artistId           = jsonSong.getInt("artistId");
            artist             = jsonSong.getString("artist");
            code               = jsonSong.getInt("code");
            smsNumber          = jsonSong.getInt("smsNumber");
            price              = jsonSong.getInt("price");
            fPrice             = jsonSong.getDouble("fPrice");
            prolongationPrice  = jsonSong.getInt("prolongationPrice");
            fProlongationPrice = jsonSong.getDouble("fProlongationPrice");
            purchasePeriod     = jsonSong.getString("purchasePeriod");
            position           = jsonSong.getInt("position");
            picUrl             = jsonSong.getString("picUrl");
            demoUrl            = jsonSong.getString("demoUrl");
            tags               = new SongTag((JSONObject) jsonSong.getJSONArray("tags").get(0));
            active             = jsonSong.getBoolean("active");
            relevance          = jsonSong.getInt("relevance");
            melodyId           = jsonSong.getInt("melodyId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getArtistId() {
        return artistId;
    }

    public String getArtist() {
        return artist;
    }

    public int getCode() {
        return code;
    }

    public int getSmsNumber() {
        return smsNumber;
    }

    public int getPrice() {
        return price;
    }

    public double getfPrice() {
        return fPrice;
    }

    public int getProlongationPrice() {
        return prolongationPrice;
    }

    public double getfProlongationPrice() {
        return fProlongationPrice;
    }

    public String getPurchasePeriod() {
        return purchasePeriod;
    }

    public int getPosition() {
        return position;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public String getDemoUrl() {
        return demoUrl;
    }

    public SongTag getTags() {
        return tags;
    }

    public boolean isActive() {
        return active;
    }

    public int getRelevance() {
        return relevance;
    }

    public int getMelodyId() {
        return melodyId;
    }
}
