package com.intech_global.intechplayer.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vladimir on 02.04.16.
 */
public class SongTag {

    private int     id;
    private String  title;
    private boolean limitedVisibility;
    private int     position;
    private boolean isFullCatalogEnabled;
    private int     topMelodiesCount;
    private boolean isBlockDisplayMode;

    public SongTag(JSONObject jsonTag) {
        try {
            id                   = jsonTag.getInt("id");
            title                = jsonTag.getString("title");
            limitedVisibility    = jsonTag.getBoolean("limitedVisibility");
            position             = jsonTag.getInt("position");
            isFullCatalogEnabled = jsonTag.getBoolean("isFullCatalogEnabled");
            topMelodiesCount      = jsonTag.getInt("topMelodiesCount");
            isBlockDisplayMode   = jsonTag.getBoolean("isBlockDisplayMode");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isLimitedVisibility() {
        return limitedVisibility;
    }

    public int getPosition() {
        return position;
    }

    public boolean isFullCatalogEnabled() {
        return isFullCatalogEnabled;
    }

    public int getTopMelodiesCount() {
        return topMelodiesCount;
    }

    public boolean isBlockDisplayMode() {
        return isBlockDisplayMode;
    }
}
