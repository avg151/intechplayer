package com.intech_global.intechplayer.view.song.grid;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.intech_global.intechplayer.R;
import com.intech_global.intechplayer.activities.PlayerActivity;
import com.intech_global.intechplayer.models.Song;

/**
 * Created by vladimir on 04.04.16.
 */
public class SongsGridFragment extends Fragment implements AdapterView.OnItemClickListener {
    Intent intent;
    Adapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.songs_grid_fragment, null);
    }

    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
        final GridView g = (GridView) getView().findViewById(R.id.gridView1);
        g.setOnItemClickListener(this);
        g.setAdapter(adapter);
        View view = getView();
        if (view != null) {
            ProgressBar progressBar = (ProgressBar) getView().findViewById(R.id.songs_grid_fragment_progress);
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
        Song song = (Song) adapter.getItem(position);

        if (intent == null) {
            intent = new Intent(getActivity(), PlayerActivity.class);
        }
        intent.putExtra(PlayerActivity.IMAGE_URL, song.getPicUrl());
        intent.putExtra(PlayerActivity.DEMO_URL,  song.getDemoUrl());
        intent.putExtra(PlayerActivity.TITLE,     song.getTitle());
        intent.putExtra(PlayerActivity.ARTIST,    song.getArtist());
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }
}
