package com.intech_global.intechplayer.view.song.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.intech_global.intechplayer.R;
import com.intech_global.intechplayer.models.Song;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by vladimir on 03.04.16.
 */
public class SongsListAdapter extends BaseAdapter {

    private ArrayList<Song> songs;
    LayoutInflater lInflater;
    Context context;
    private ListLoadingListener pageLoadingListener;

    private final String TAG_LOADING = "loading";
    private final String TAG_DATA = "data";

    public SongsListAdapter(Context context, ArrayList<Song> songs) {
        this.songs = songs;
        this.context = context;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return songs.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        if (position == songs.size()) {
            return null;
        } else {
            return songs.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (position == songs.size()) {
            if (view == null || view.getTag() == TAG_DATA) {
                view = lInflater.inflate(R.layout.loading_list_item, parent, false);
                view.setTag(TAG_LOADING);
                pageLoadingListener.onLoadNextPage();
            }
        } else {
            if (view == null || view.getTag() == TAG_LOADING) {
                view = lInflater.inflate(R.layout.song_list_item, parent, false);
                view.setTag(TAG_DATA);
            }

            Song s = (Song) getItem(position);

            ((TextView) view.findViewById(R.id.tvTitle)).setText(s.getTitle());
            ((TextView) view.findViewById(R.id.tvAuthor)).setText(s.getArtist());

            Picasso.with(context).load(s.getPicUrl()).into((ImageView) view.findViewById(R.id.icon));
        }
        return view;
    }

    public void setPageLoadingListener(ListLoadingListener pageLoadingListener) {
        this.pageLoadingListener = pageLoadingListener;
    }

    public void add(ArrayList<Song> songs) {
        this.songs.addAll(songs);
    }

    public interface ListLoadingListener {
        void onLoadNextPage();
    }
}
