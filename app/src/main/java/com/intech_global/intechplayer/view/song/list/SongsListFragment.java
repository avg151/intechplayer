package com.intech_global.intechplayer.view.song.list;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;

import com.intech_global.intechplayer.R;
import com.intech_global.intechplayer.activities.PlayerActivity;
import com.intech_global.intechplayer.models.Song;

/**
 * Created by vladimir on 03.04.16.
 */
public class SongsListFragment extends ListFragment implements AdapterView.OnItemClickListener {
    Intent intent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.songs_list_fragment, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setOnItemClickListener(this);
    }

    public void setListAdapter(BaseAdapter adapter) {
        super.setListAdapter(adapter);
        View view = getView();
        if (view != null) {
            ProgressBar progressBar = (ProgressBar) getView().findViewById(R.id.songs_list_fragment_progress);
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
        Song song = (Song) getListAdapter().getItem(position);

        if (intent == null) {
            intent = new Intent(getActivity(), PlayerActivity.class);
        }
        intent.putExtra(PlayerActivity.IMAGE_URL, song.getPicUrl());
        intent.putExtra(PlayerActivity.DEMO_URL,  song.getDemoUrl());
        intent.putExtra(PlayerActivity.TITLE,     song.getTitle());
        intent.putExtra(PlayerActivity.ARTIST,    song.getArtist());
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }
}
